Simple Vending Machine Demo with a SpringBoot front end.

This can be built via maven, using

mvn clean package

It can also be run from the command line like this:

mvn spring-boot:run

It's not 100% complete, but has good unit test coverage, including a sample controller test - if time allowed, we'd do more tests for this controller!