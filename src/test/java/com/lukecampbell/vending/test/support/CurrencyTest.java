package com.lukecampbell.vending.test.support;

import java.util.ArrayList;

import org.junit.Test;

import com.lukecampbell.vending.support.Currency;
import com.lukecampbell.vending.support.PennyCounter;
import com.lukecampbell.vending.support.Currency.Coins;

import junit.framework.TestCase;

public class CurrencyTest extends TestCase {
	private PennyCounter pennies;
	
	boolean containsTen = false;
	boolean containsFifty = false;
	boolean containsTwenty = false;
	boolean containsPound = false;
	boolean containsPenny = false;
	private int countPound;
	private int countFifty;
	private int countTwenty;
	private int countTen;
	private int countPenny;
	
	@Override
	protected void setUp() {
		pennies = new PennyCounter();
		containsTen = false;
		containsFifty = false;
		containsTwenty = false;
		containsPound = false;
		containsPenny = false;
		countPound = 0;
		countFifty = 0;
		countTwenty = 0;
		countTen = 0;
		countPenny = 0;
	}
	
	@Override
	protected void tearDown() {
		
	}
	
	private void checkForCoin(Coins coin) {
		pennies.checkForCoin(coin);
		
	}
	
	private int makeBalance(int numberPennies, int numberTens, int numberTwenties, int numberFifties,
			int numberPounds) {
		return pennies.makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
	}
	
	private void pincherResults() {
		containsTen = pennies.isContainsTen();
		containsFifty = pennies.isContainsFifty();
		containsTwenty = pennies.isContainsTwenty();
		containsPound = pennies.isContainsPound();
		containsPenny = pennies.isContainsPenny();
		countPound = pennies.getCountPound();
		countFifty = pennies.getCountFifty();
		countTwenty = pennies.getCountTwenty();
		countTen = pennies.getCountTen();
		countPenny = pennies.getCountPenny();
	}
	
	@Test
	public void testValidPenny() {
		boolean expected = false;
		boolean valid = Currency.isValid(Coins.PENNY);
		assertEquals(valid, expected);
	}
	
	@Test
	public void testValidCoins() {
		boolean expected = true;
		boolean valid = Currency.isValid(Coins.TEN);
		assertEquals(valid, expected);
		
		valid = Currency.isValid(Coins.TWENTY);
		assertEquals(valid, expected);
		
		valid = Currency.isValid(Coins.FIFTY);
		assertEquals(valid, expected);
		
		valid = Currency.isValid(Coins.POUND);
		assertEquals(valid, expected);
	}
	
	@Test
	public void testSimpleRefund() {
		int numberPennies = 0;
		int numberTens = 1;
		int numberTwenties = 0;
		int numberFifties = 1;
		int numberPounds = 0;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		
		ArrayList<Coins> coins = Currency.getBalance(balance);
		int size = coins.size();
		int expectedSize = 2;
		assertEquals(expectedSize, size);
		
		for(Coins coin: coins) {
			System.out.print("\n* Change: "+coin.toString());
			checkForCoin(coin);
		}
		pincherResults();
		assertFalse(containsPound);
		assertTrue(containsFifty);
		assertFalse(containsTwenty);
		assertTrue(containsTen);
		assertFalse(containsPenny);
		assertEquals(countPound, numberPounds);
		assertEquals(countFifty, numberFifties);
		assertEquals(countTwenty, numberTwenties);
		assertEquals(countTen, numberTens);
		assertEquals(countPenny, numberPennies);
	}
	
	
	
	

	@Test
	public void testRefundComplex() {
		int numberPennies = 7;
		int numberTens = 1;
		int numberTwenties = 0;
		int numberFifties = 1;
		int numberPounds = 1;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		ArrayList<Coins> coins = Currency.getBalance(balance);
		int size = coins.size();
		int expectedSize = numberPounds+numberFifties+numberTwenties+numberTens+numberPennies;
		assertEquals(expectedSize, size);
		for(Coins coin: coins) {
			System.out.print("\n* Change: "+coin.toString());
			checkForCoin(coin);
		}
		pincherResults();
		assertTrue(containsPound);
		assertTrue(containsFifty);
		assertFalse(containsTwenty);
		assertTrue(containsTen);
		assertTrue(containsPenny);
		assertEquals(countPound, numberPounds);
		assertEquals(countFifty, numberFifties);
		assertEquals(countTwenty, numberTwenties);
		assertEquals(countTen, numberTens);
		assertEquals(countPenny, numberPennies);
	}
}
