package com.lukecampbell.vending.test.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lukecampbell.vending.model.VendingModel;
import com.lukecampbell.vending.product.Product;
import com.lukecampbell.vending.support.Constants;
import com.lukecampbell.vending.support.Currency;
import com.lukecampbell.vending.support.DemoConstants;
import com.lukecampbell.vending.support.PennyCounter;
import com.lukecampbell.vending.support.Currency.Coins;

import junit.framework.TestCase;

public class VendingModelTest extends TestCase {
	private VendingModel model;
	private PennyCounter pennies;
	boolean changeContainsTen = false;
	boolean changeContainsFifty = false;
	boolean changeContainsTwenty = false;
	boolean changeContainsPound = false;
	boolean changeContainsPenny = false;
	private int countPound;
	private int countFifty;
	private int countTwenty;
	private int countTen;
	private int countPenny;
	private Product product1;
	private Product product2;
	private Product product3;
	private String product1text = DemoConstants.product1text;
	private int product1Cost = DemoConstants.product1Cost;
	private String product2text = DemoConstants.product2text;
	private int product2Cost = DemoConstants.product2Cost;
	private String product3text = DemoConstants.product3text;
	private int product3Cost = DemoConstants.product3Cost;
	
	@Before
    protected void setUp() throws Exception {
        changeContainsTen = false;
		changeContainsFifty = false;
		changeContainsTwenty = false;
		changeContainsPound = false;
		changeContainsPenny = false;
		countPound = 0;
		countFifty = 0;
		countTwenty = 0;
		countTen = 0;
		countPenny = 0;
        pennies = new PennyCounter();
		product1 = new Product(product1text, product1Cost);
		product2 = new Product(product2text, product2Cost);
		product3 = new Product(product3text, product3Cost);
		model = new VendingModel(product1, product2, product3);
		// (obviously : could also create array list and call service locator, to support many products!)
		//machineInstance.setOn();
		
    }
	
	@After
	public void TearDown() {
		model.setOff();
	}
	
	
	
	@Test
	public void defaultStateIsOff() {
		assertFalse(model.isOn());
	}
	
	@Test
	public void turnsOn() {
		model.setOn();
		assertTrue(model.isOn());		
	}
	
	@Test
	public void checkProducts() {
		//machineInstance.setOn();
		List<Product> myProducts = model.getProducts();
		int size = myProducts.size();
		int expectedSize = 3;
		assertEquals(expectedSize, size);
	}
	
	@Test
	public void testInsertCoins() {
		model.setOn();
		Boolean success = model.insertCoins(Coins.TEN);	
		assertTrue(success);
		int expectedBalance = Coins.TEN.value();
		assertEquals(expectedBalance, model.getBalance());
	}
	
	@Test
	public void testInsertInvalidCoins() {
		model.setOn();
		Boolean success = model.insertCoins(Coins.PENNY);	
		assertFalse(success);
		int expectedBalance = 0;
		assertEquals(expectedBalance, model.getBalance());
	}
	
	@Test
	public void testInsertTwoCoins() {
		model.setOn();
		int numberPennies = 0;
		int numberTens = 1;
		int numberTwenties = 0;
		int numberFifties = 1;
		int numberPounds = 0;
		
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		
		int expectedBalance = Coins.TEN.value()+Coins.FIFTY.value();
		assertEquals(expectedBalance, model.getBalance());
	}
	
	
	@Test
	public void testSimpleRefund() {
		int numberPennies = 0;
		int numberTens = 1;
		int numberTwenties = 0;
		int numberFifties = 1;
		int numberPounds = 0;
		model.setOn();
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		
		List<Coins> coins = Currency.getBalance(balance);
		int size = coins.size();
		int expectedSize = 2;
		assertEquals(expectedSize, size);
		
		for(Coins coin: coins) {
			pennies.checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		assertFalse(changeContainsPound);
		assertTrue(changeContainsFifty);
		assertFalse(changeContainsTwenty);
		assertTrue(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, numberPounds);
		assertEquals(countFifty, numberFifties);
		assertEquals(countTwenty, numberTwenties);
		assertEquals(countTen, numberTens);
		assertEquals(countPenny, numberPennies);
		
		Optional<List<Coins>> coinsRefundedContainer = model.refundCoins();
		assertTrue(coinsRefundedContainer.isPresent());
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		resetMoneyCounter();
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		assertFalse(changeContainsPound);
		assertTrue(changeContainsFifty);
		assertFalse(changeContainsTwenty);
		assertTrue(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, numberPounds);
		assertEquals(countFifty, numberFifties);
		assertEquals(countTwenty, numberTwenties);
		assertEquals(countTen, numberTens);
		assertEquals(countPenny, numberPennies);
	}
	
	@Test
	public void testVendWithZeroBalance() {
		model.setOn();
		Boolean purchased = model.purchaseProduct(product1text);
		assertFalse(purchased);
	}
	
	@Test
	public void testVendProductAExactChange() {
		model.setOn();
		int numberPennies = 0;
		int numberTens = 1;
		int numberTwenties = 0;
		int numberFifties = 1;
		int numberPounds = 0;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = model.purchaseProduct(product1text);
		assertTrue(purchased);
		int expectedBalance = balance - product1Cost;
		assertEquals(expectedBalance, model.getBalance());
		Optional<List<Coins>> coinsRefundedContainer = model.refundCoins();
		assertTrue(coinsRefundedContainer.isPresent());
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		resetMoneyCounter();
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		assertFalse(changeContainsPound);
		assertFalse(changeContainsFifty);
		assertFalse(changeContainsTwenty);
		assertFalse(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, Constants.ZERO);
		assertEquals(countFifty, Constants.ZERO);
		assertEquals(countTwenty, Constants.ZERO);
		assertEquals(countTen, Constants.ZERO);
		assertEquals(countPenny, Constants.ZERO);
		
	}
	
	@Test
	public void testVendProductAWithChange() {
		model.setOn();
		int numberPennies = 0;
		int numberTens = 2;
		int numberTwenties = 0;
		int numberFifties = 1;
		int numberPounds = 0;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = model.purchaseProduct(product1text);
		assertTrue(purchased);
		int expectedBalance = balance - product1Cost;
		assertEquals(expectedBalance, model.getBalance());
		Optional<List<Coins>> coinsRefundedContainer = model.refundCoins();
		assertTrue(coinsRefundedContainer.isPresent());
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		resetMoneyCounter();
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		int expectedTens = 1;
		assertFalse(changeContainsPound);
		assertFalse(changeContainsFifty);
		assertFalse(changeContainsTwenty);
		assertTrue(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, Constants.ZERO);
		assertEquals(countFifty, Constants.ZERO);
		assertEquals(countTwenty, Constants.ZERO);
		assertEquals(countTen, expectedTens);
		assertEquals(countPenny, Constants.ZERO);
		// shouldn't be able to purchase twice
		purchased = model.purchaseProduct(product1text);
		assertFalse(purchased);
	}
	
	@Test
	public void testVendProductBExactChange() {
		model.setOn();
		int numberPennies = 0;
		int numberTens = 1;
		int numberTwenties = 0;
		int numberFifties = 0;
		int numberPounds = 1;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = model.purchaseProduct(product2text);
		assertTrue(purchased);
		int expectedBalance = balance - product2Cost;
		assertEquals(expectedBalance, model.getBalance());
		// shouldn't be able to purchase twice
		purchased = model.purchaseProduct(product2text);
		assertFalse(purchased);
	}
	
	@Test
	public void testVendProductBExactChangeWithFifties() {
		model.setOn();
		int numberPennies = 0;
		int numberTens = 1;
		int numberTwenties = 0;
		int numberFifties = 2;
		int numberPounds = 0;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = model.purchaseProduct(product2text);
		assertTrue(purchased);
		int expectedBalance = balance - product2Cost;
		assertEquals(expectedBalance, model.getBalance());
		// shouldn't be able to purchase twice
		purchased = model.purchaseProduct(product2text);
		assertFalse(purchased);
	}
	
	@Test
	public void testVendProductBTwentyChange() {
		model.setOn();
		int numberPennies = 0;
		int numberTens = 0;
		int numberTwenties = 1;
		int numberFifties = 2;
		int numberPounds = 0;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = model.purchaseProduct(product2text);
		assertTrue(purchased);
		int expectedBalance = balance - product2Cost;
		assertEquals(expectedBalance, model.getBalance());
		// shouldn't be able to purchase twice
		purchased = model.purchaseProduct(product2text);
		assertFalse(purchased);
		Optional<List<Coins>> coinsRefundedContainer = model.refundCoins();
		assertTrue(coinsRefundedContainer.isPresent());
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		resetMoneyCounter();
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		int expectedTwenties = 1;
		assertFalse(changeContainsPound);
		assertFalse(changeContainsFifty);
		assertTrue(changeContainsTwenty);
		assertFalse(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, Constants.ZERO);
		assertEquals(countFifty, Constants.ZERO);
		assertEquals(countTwenty, expectedTwenties);
		assertEquals(countTen, Constants.ZERO);
		assertEquals(countPenny, Constants.ZERO);
	}
	
	@Test
	public void testVendProductC() {
		model.setOn();
		int numberPennies = 0;
		int numberTens = 2;
		int numberTwenties = 0;
		int numberFifties = 3;
		int numberPounds = 0;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = model.purchaseProduct(product3text);
		assertTrue(purchased);
		int expectedBalance = balance - product3Cost;
		assertEquals(expectedBalance, model.getBalance());
		// shouldn't be able to purchase twice
		purchased = model.purchaseProduct(product2text);
		assertFalse(purchased);
	}
	
	@Test
	public void testVendProductCTenChange() {
		model.setOn();
		int numberPennies = 0;
		int numberTens = 3;
		int numberTwenties = 0;
		int numberFifties = 3;
		int numberPounds = 0;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = model.purchaseProduct(product3text);
		assertTrue(purchased);
		int expectedBalance = balance - product3Cost;
		assertEquals(expectedBalance, model.getBalance());
		// shouldn't be able to purchase twice
		purchased = model.purchaseProduct(product2text);
		assertFalse(purchased);
		Optional<List<Coins>> coinsRefundedContainer = model.refundCoins();
		assertTrue(coinsRefundedContainer.isPresent());
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		resetMoneyCounter();
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		int expectedTens = 1;
		assertFalse(changeContainsPound);
		assertFalse(changeContainsFifty);
		assertFalse(changeContainsTwenty);
		assertTrue(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, Constants.ZERO);
		assertEquals(countFifty, Constants.ZERO);
		assertEquals(countTwenty, Constants.ZERO);
		assertEquals(countTen, expectedTens);
		assertEquals(countPenny, Constants.ZERO);
	}
	
	
	@Test
	public void testVendProductAllNoChange() {
		model.setOn();
		int numberPennies = 0;
		int numberTens = 1;
		int numberTwenties = 1;
		int numberFifties = 0;
		int numberPounds = 3;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = model.purchaseProduct(product3text);
		assertTrue(purchased);
		purchased = model.purchaseProduct(product2text);
		assertTrue(purchased);
		purchased = model.purchaseProduct(product1text);
		assertTrue(purchased);
		int expectedBalance = balance - product3Cost - product2Cost - product1Cost;
		assertEquals(expectedBalance, model.getBalance());
		Optional<List<Coins>> coinsRefundedContainer = model.refundCoins();
		assertTrue(coinsRefundedContainer.isPresent());
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		resetMoneyCounter();
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		assertFalse(changeContainsPound);
		assertFalse(changeContainsFifty);
		assertFalse(changeContainsTwenty);
		assertFalse(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, Constants.ZERO);
		assertEquals(countFifty, Constants.ZERO);
		assertEquals(countTwenty, Constants.ZERO);
		assertEquals(countTen, Constants.ZERO);
		assertEquals(countPenny, Constants.ZERO);
		
	}
	
	@Test
	public void testVendProductAllTenChange() {
		model.setOn();
		int numberPennies = 0;
		int numberTens = 2;
		int numberTwenties = 1;
		int numberFifties = 0;
		int numberPounds = 3;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = model.purchaseProduct(product3text);
		assertTrue(purchased);
		purchased = model.purchaseProduct(product2text);
		assertTrue(purchased);
		purchased = model.purchaseProduct(product1text);
		assertTrue(purchased);
		int expectedBalance = balance - product3Cost - product2Cost - product1Cost;
		assertEquals(expectedBalance, model.getBalance());
		Optional<List<Coins>> coinsRefundedContainer = model.refundCoins();
		assertTrue(coinsRefundedContainer.isPresent());
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		resetMoneyCounter();
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		int tenExpected = 1;
		assertFalse(changeContainsPound);
		assertFalse(changeContainsFifty);
		assertFalse(changeContainsTwenty);
		assertTrue(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, Constants.ZERO);
		assertEquals(countFifty, Constants.ZERO);
		assertEquals(countTwenty, Constants.ZERO);
		assertEquals(countTen, tenExpected);
		assertEquals(countPenny, Constants.ZERO);
		
	}
	
	@Test
	public void testVendProductAllFiftyChange() {
		model.setOn();
		int numberPennies = 0;
		int numberTens = 1;
		int numberTwenties = 1;
		int numberFifties = 1;
		int numberPounds = 3;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = model.purchaseProduct(product3text);
		assertTrue(purchased);
		purchased = model.purchaseProduct(product2text);
		assertTrue(purchased);
		purchased = model.purchaseProduct(product1text);
		assertTrue(purchased);
		int expectedBalance = balance - product3Cost - product2Cost - product1Cost;
		assertEquals(expectedBalance, model.getBalance());
		Optional<List<Coins>> coinsRefundedContainer = model.refundCoins();
		assertTrue(coinsRefundedContainer.isPresent());
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		resetMoneyCounter();
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		int fiftyExpected = 1;
		assertFalse(changeContainsPound);
		assertTrue(changeContainsFifty);
		assertFalse(changeContainsTwenty);
		assertFalse(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, Constants.ZERO);
		assertEquals(countFifty, fiftyExpected);
		assertEquals(countTwenty, Constants.ZERO);
		assertEquals(countTen, Constants.ZERO);
		assertEquals(countPenny, Constants.ZERO);	
	}
	
	/* misc functions to support unit tests */
	
	private void retrieveResultsFromMoneyCounter() {
		changeContainsTen = pennies.isContainsTen();
		changeContainsFifty = pennies.isContainsFifty();
		changeContainsTwenty = pennies.isContainsTwenty();
		changeContainsPound = pennies.isContainsPound();
		changeContainsPenny = pennies.isContainsPenny();
		countPound = pennies.getCountPound();
		countFifty = pennies.getCountFifty();
		countTwenty = pennies.getCountTwenty();
		countTen = pennies.getCountTen();
		countPenny = pennies.getCountPenny();
	}
	
	private int makeBalance(int numberPennies, int numberTens, int numberTwenties, int numberFifties,
			int numberPounds) {
		for(int i = 0; i < numberPounds; i++) {
			Boolean itsOk = model.insertCoins(Coins.POUND);
			assertTrue(itsOk);
		}
		for(int i = 0; i < numberFifties; i++) {
			Boolean itsOk = model.insertCoins(Coins.FIFTY);
			assertTrue(itsOk);
		}
		for(int i = 0; i < numberTwenties; i++) {
			Boolean itsOk = model.insertCoins(Coins.TWENTY);
			assertTrue(itsOk);
		}
		for(int i = 0; i < numberTens; i++) {
			Boolean itsOk = model.insertCoins(Coins.TEN);
			assertTrue(itsOk);
		}
		return model.getBalance();
	}
	
	private void checkForCoin(Coins coin) {
		pennies.checkForCoin(coin);
	}
	
	private void resetMoneyCounter() {
		pennies.reset();
	}
}
