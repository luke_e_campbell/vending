package com.lukecampbell.vending.test.machine;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.lukecampbell.vending.application.controller.VendingController;
import com.lukecampbell.vending.model.VendingModel;
import com.lukecampbell.vending.product.Product;
import com.lukecampbell.vending.service.iface.IVendingMachineService;
import com.lukecampbell.vending.support.Constants;
import com.lukecampbell.vending.support.Currency;
import com.lukecampbell.vending.support.Currency.Coins;
import com.lukecampbell.vending.support.DemoConstants;
import com.lukecampbell.vending.support.PennyCounter;

import junit.framework.TestCase;

/**
 * Unit tests for {@link VendingMachineService}
 */
@RunWith(SpringRunner.class)
@WebMvcTest(VendingController.class)
public class VendingMachineServiceTest {
	private VendingModel model;
	
	@Autowired
	private IVendingMachineService machineInstance;
	
	private PennyCounter pennies;
	boolean changeContainsTen = false;
	boolean changeContainsFifty = false;
	boolean changeContainsTwenty = false;
	boolean changeContainsPound = false;
	boolean changeContainsPenny = false;
	private int countPound;
	private int countFifty;
	private int countTwenty;
	private int countTen;
	private int countPenny;
	private Product product1;
	private Product product2;
	private Product product3;
	private String product1text = DemoConstants.product1text;
	private int product1Cost = DemoConstants.product1Cost;
	private String product2text = DemoConstants.product2text;
	private int product2Cost = DemoConstants.product2Cost;
	private String product3text = DemoConstants.product3text;
	private int product3Cost = DemoConstants.product3Cost;
	
	@Before
    public void setUp() throws Exception {
		model = new VendingModel();
        changeContainsTen = false;
		changeContainsFifty = false;
		changeContainsTwenty = false;
		changeContainsPound = false;
		changeContainsPenny = false;
		countPound = 0;
		countFifty = 0;
		countTwenty = 0;
		countTen = 0;
		countPenny = 0;
        pennies = new PennyCounter();
		product1 = new Product(product1text, product1Cost);
		product2 = new Product(product2text, product2Cost);
		product3 = new Product(product3text, product3Cost);
		List<Product> list = new ArrayList<Product>();
		list.add(product1);
		list.add(product2);
		list.add(product3);
		machineInstance.addProducts(model, list);
		// (obviously : could also create array list and call service locator, to support many products!)
		//machineInstance.setOn(model);
		
    }
	
	@After
	public void TearDown() {
		machineInstance.setOff(model);
	}
	
	
	
	@Test
	public void defaultStateIsOff() {
		assertFalse(machineInstance.isOn(model));
	}
	
	@Test
	public void turnsOn() {
		machineInstance.setOn(model);
		assertTrue(machineInstance.isOn(model));		
	}
	
	@Test
	public void checkProducts() {
		machineInstance.setOn(model);
		List<Product> myProducts = machineInstance.getProducts(model);
		int size = myProducts.size();
		int expectedSize = 3;
		assertEquals(expectedSize, size);
	}
	
	@Test
	public void testInsertCoins() {
		machineInstance.setOn(model);
		Boolean success = machineInstance.insertCoins(model, Coins.TEN);	
		assertTrue(success);
		int expectedBalance = Coins.TEN.value();
		assertEquals(expectedBalance, machineInstance.getBalance(model));
	}
	
	@Test
	public void testInsertInvalidCoins() {
		machineInstance.setOn(model);
		Boolean success = machineInstance.insertCoins(model, Coins.PENNY);	
		assertFalse(success);
		int expectedBalance = 0;
		assertEquals(expectedBalance, machineInstance.getBalance(model));
	}
	
	@Test
	public void testInsertTwoCoins() {
		machineInstance.setOn(model);
		int numberPennies = 0;
		int numberTens = 1;
		int numberTwenties = 0;
		int numberFifties = 1;
		int numberPounds = 0;
		
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		
		int expectedBalance = Coins.TEN.value()+Coins.FIFTY.value();
		assertEquals(expectedBalance, machineInstance.getBalance(model));
	}
	
	
	@Test
	public void testSimpleRefund() {
		int numberPennies = 0;
		int numberTens = 1;
		int numberTwenties = 0;
		int numberFifties = 1;
		int numberPounds = 0;
		machineInstance.setOn(model);
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		
		ArrayList<Coins> coins = Currency.getBalance(balance);
		int size = coins.size();
		int expectedSize = 2;
		assertEquals(expectedSize, size);
		
		for(Coins coin: coins) {
			pennies.checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		assertFalse(changeContainsPound);
		assertTrue(changeContainsFifty);
		assertFalse(changeContainsTwenty);
		assertTrue(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, numberPounds);
		assertEquals(countFifty, numberFifties);
		assertEquals(countTwenty, numberTwenties);
		assertEquals(countTen, numberTens);
		assertEquals(countPenny, numberPennies);
		
		Optional<List<Coins>> coinsRefundedContainer = machineInstance.refundCoins(model);
		assertTrue(coinsRefundedContainer.isPresent());
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		resetMoneyCounter();
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		assertFalse(changeContainsPound);
		assertTrue(changeContainsFifty);
		assertFalse(changeContainsTwenty);
		assertTrue(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, numberPounds);
		assertEquals(countFifty, numberFifties);
		assertEquals(countTwenty, numberTwenties);
		assertEquals(countTen, numberTens);
		assertEquals(countPenny, numberPennies);
	}
	
	@Test
	public void testVendWithZeroBalance() {
		machineInstance.setOn(model);
		Boolean purchased = machineInstance.purchaseProduct(model, product1text);
		assertFalse(purchased);
	}
	
	@Test
	public void testVendProductAExactChange() {
		machineInstance.setOn(model);
		int numberPennies = 0;
		int numberTens = 1;
		int numberTwenties = 0;
		int numberFifties = 1;
		int numberPounds = 0;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = machineInstance.purchaseProduct(model, product1text);
		assertTrue(purchased);
		int expectedBalance = balance - product1Cost;
		assertEquals(expectedBalance, machineInstance.getBalance(model));
		Optional<List<Coins>> coinsRefundedContainer = machineInstance.refundCoins(model);
		assertTrue(coinsRefundedContainer.isPresent());
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		resetMoneyCounter();
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		
		assertFalse(changeContainsPound);
		assertFalse(changeContainsFifty);
		assertFalse(changeContainsTwenty);
		assertFalse(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, Constants.ZERO);
		assertEquals(countFifty, Constants.ZERO);
		assertEquals(countTwenty, Constants.ZERO);
		assertEquals(countTen, Constants.ZERO);
		assertEquals(countPenny, Constants.ZERO);
		
	}
	
	@Test
	public void testVendProductAWithChange() {
		machineInstance.setOn(model);
		int numberPennies = 0;
		int numberTens = 2;
		int numberTwenties = 0;
		int numberFifties = 1;
		int numberPounds = 0;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = machineInstance.purchaseProduct(model, product1text);
		assertTrue(purchased);
		int expectedBalance = balance - product1Cost;
		assertEquals(expectedBalance, machineInstance.getBalance(model));
		Optional<List<Coins>> coinsRefundedContainer = machineInstance.refundCoins(model);
		assertTrue(coinsRefundedContainer.isPresent());
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		resetMoneyCounter();
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		int expectedTens = 1;
		assertFalse(changeContainsPound);
		assertFalse(changeContainsFifty);
		assertFalse(changeContainsTwenty);
		assertTrue(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, Constants.ZERO);
		assertEquals(countFifty, Constants.ZERO);
		assertEquals(countTwenty, Constants.ZERO);
		assertEquals(countTen, expectedTens);
		assertEquals(countPenny, Constants.ZERO);
		// shouldn't be able to purchase twice
		purchased = machineInstance.purchaseProduct(model, product1text);
		assertFalse(purchased);
	}
	
	@Test
	public void testVendProductBExactChange() {
		machineInstance.setOn(model);
		int numberPennies = 0;
		int numberTens = 1;
		int numberTwenties = 0;
		int numberFifties = 0;
		int numberPounds = 1;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = machineInstance.purchaseProduct(model, product2text);
		assertTrue(purchased);
		int expectedBalance = balance - product2Cost;
		assertEquals(expectedBalance, machineInstance.getBalance(model));
		// shouldn't be able to purchase twice
		purchased = machineInstance.purchaseProduct(model, product2text);
		assertFalse(purchased);
	}
	
	@Test
	public void testVendProductBExactChangeWithFifties() {
		machineInstance.setOn(model);
		int numberPennies = 0;
		int numberTens = 1;
		int numberTwenties = 0;
		int numberFifties = 2;
		int numberPounds = 0;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = machineInstance.purchaseProduct(model, product2text);
		assertTrue(purchased);
		int expectedBalance = balance - product2Cost;
		assertEquals(expectedBalance, machineInstance.getBalance(model));
		// shouldn't be able to purchase twice
		purchased = machineInstance.purchaseProduct(model, product2text);
		assertFalse(purchased);
	}
	
	@Test
	public void testVendProductBTwentyChange() {
		machineInstance.setOn(model);
		int numberPennies = 0;
		int numberTens = 0;
		int numberTwenties = 1;
		int numberFifties = 2;
		int numberPounds = 0;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = machineInstance.purchaseProduct(model, product2text);
		assertTrue(purchased);
		int expectedBalance = balance - product2Cost;
		assertEquals(expectedBalance, machineInstance.getBalance(model));
		// shouldn't be able to purchase twice
		purchased = machineInstance.purchaseProduct(model, product2text);
		assertFalse(purchased);
		Optional<List<Coins>> coinsRefundedContainer = machineInstance.refundCoins(model);
		assertTrue(coinsRefundedContainer.isPresent());
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		resetMoneyCounter();
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		int expectedTwenties = 1;
		assertFalse(changeContainsPound);
		assertFalse(changeContainsFifty);
		assertTrue(changeContainsTwenty);
		assertFalse(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, Constants.ZERO);
		assertEquals(countFifty, Constants.ZERO);
		assertEquals(countTwenty, expectedTwenties);
		assertEquals(countTen, Constants.ZERO);
		assertEquals(countPenny, Constants.ZERO);
	}
	
	@Test
	public void testVendProductC() {
		machineInstance.setOn(model);
		int numberPennies = 0;
		int numberTens = 2;
		int numberTwenties = 0;
		int numberFifties = 3;
		int numberPounds = 0;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = machineInstance.purchaseProduct(model, product3text);
		assertTrue(purchased);
		int expectedBalance = balance - product3Cost;
		assertEquals(expectedBalance, machineInstance.getBalance(model));
		// shouldn't be able to purchase twice
		purchased = machineInstance.purchaseProduct(model, product2text);
		assertFalse(purchased);
	}
	
	@Test
	public void testVendProductCTenChange() {
		machineInstance.setOn(model);
		int numberPennies = 0;
		int numberTens = 3;
		int numberTwenties = 0;
		int numberFifties = 3;
		int numberPounds = 0;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = machineInstance.purchaseProduct(model, product3text);
		assertTrue(purchased);
		int expectedBalance = balance - product3Cost;
		assertEquals(expectedBalance, machineInstance.getBalance(model));
		// shouldn't be able to purchase twice
		purchased = machineInstance.purchaseProduct(model, product2text);
		assertFalse(purchased);
		Optional<List<Coins>> coinsRefundedContainer = machineInstance.refundCoins(model);
		assertTrue(coinsRefundedContainer.isPresent());
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		resetMoneyCounter();
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		int expectedTens = 1;
		assertFalse(changeContainsPound);
		assertFalse(changeContainsFifty);
		assertFalse(changeContainsTwenty);
		assertTrue(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, Constants.ZERO);
		assertEquals(countFifty, Constants.ZERO);
		assertEquals(countTwenty, Constants.ZERO);
		assertEquals(countTen, expectedTens);
		assertEquals(countPenny, Constants.ZERO);
	}
	
	
	@Test
	public void testVendProductAllNoChange() {
		machineInstance.setOn(model);
		int numberPennies = 0;
		int numberTens = 1;
		int numberTwenties = 1;
		int numberFifties = 0;
		int numberPounds = 3;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = machineInstance.purchaseProduct(model, product3text);
		assertTrue(purchased);
		purchased = machineInstance.purchaseProduct(model, product2text);
		assertTrue(purchased);
		purchased = machineInstance.purchaseProduct(model, product1text);
		assertTrue(purchased);
		int expectedBalance = balance - product3Cost - product2Cost - product1Cost;
		assertEquals(expectedBalance, machineInstance.getBalance(model));
		Optional<List<Coins>> coinsRefundedContainer = machineInstance.refundCoins(model);
		assertTrue(coinsRefundedContainer.isPresent());
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		resetMoneyCounter();
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		assertFalse(changeContainsPound);
		assertFalse(changeContainsFifty);
		assertFalse(changeContainsTwenty);
		assertFalse(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, Constants.ZERO);
		assertEquals(countFifty, Constants.ZERO);
		assertEquals(countTwenty, Constants.ZERO);
		assertEquals(countTen, Constants.ZERO);
		assertEquals(countPenny, Constants.ZERO);
		
	}
	
	@Test
	public void testVendProductAllTenChange() {
		machineInstance.setOn(model);
		int numberPennies = 0;
		int numberTens = 2;
		int numberTwenties = 1;
		int numberFifties = 0;
		int numberPounds = 3;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = machineInstance.purchaseProduct(model, product3text);
		assertTrue(purchased);
		purchased = machineInstance.purchaseProduct(model, product2text);
		assertTrue(purchased);
		purchased = machineInstance.purchaseProduct(model, product1text);
		assertTrue(purchased);
		int expectedBalance = balance - product3Cost - product2Cost - product1Cost;
		assertEquals(expectedBalance, machineInstance.getBalance(model));
		Optional<List<Coins>> coinsRefundedContainer = machineInstance.refundCoins(model);
		assertTrue(coinsRefundedContainer.isPresent());
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		resetMoneyCounter();
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		int tenExpected = 1;
		assertFalse(changeContainsPound);
		assertFalse(changeContainsFifty);
		assertFalse(changeContainsTwenty);
		assertTrue(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, Constants.ZERO);
		assertEquals(countFifty, Constants.ZERO);
		assertEquals(countTwenty, Constants.ZERO);
		assertEquals(countTen, tenExpected);
		assertEquals(countPenny, Constants.ZERO);
		
	}
	
	@Test
	public void testVendProductAllFiftyChange() {
		machineInstance.setOn(model);
		int numberPennies = 0;
		int numberTens = 1;
		int numberTwenties = 1;
		int numberFifties = 1;
		int numberPounds = 3;
		int balance = makeBalance(numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = machineInstance.purchaseProduct(model, product3text);
		assertTrue(purchased);
		purchased = machineInstance.purchaseProduct(model, product2text);
		assertTrue(purchased);
		purchased = machineInstance.purchaseProduct(model, product1text);
		assertTrue(purchased);
		int expectedBalance = balance - product3Cost - product2Cost - product1Cost;
		assertEquals(expectedBalance, machineInstance.getBalance(model));
		Optional<List<Coins>> coinsRefundedContainer = machineInstance.refundCoins(model);
		assertTrue(coinsRefundedContainer.isPresent());
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		resetMoneyCounter();
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		retrieveResultsFromMoneyCounter();
		int fiftyExpected = 1;
		assertFalse(changeContainsPound);
		assertTrue(changeContainsFifty);
		assertFalse(changeContainsTwenty);
		assertFalse(changeContainsTen);
		assertFalse(changeContainsPenny);
		assertEquals(countPound, Constants.ZERO);
		assertEquals(countFifty, fiftyExpected);
		assertEquals(countTwenty, Constants.ZERO);
		assertEquals(countTen, Constants.ZERO);
		assertEquals(countPenny, Constants.ZERO);	
	}
	
	/* misc functions to support unit tests */
	
	private void retrieveResultsFromMoneyCounter() {
		changeContainsTen = pennies.isContainsTen();
		changeContainsFifty = pennies.isContainsFifty();
		changeContainsTwenty = pennies.isContainsTwenty();
		changeContainsPound = pennies.isContainsPound();
		changeContainsPenny = pennies.isContainsPenny();
		countPound = pennies.getCountPound();
		countFifty = pennies.getCountFifty();
		countTwenty = pennies.getCountTwenty();
		countTen = pennies.getCountTen();
		countPenny = pennies.getCountPenny();
	}
	
	private int makeBalance(int numberPennies, int numberTens, int numberTwenties, int numberFifties,
			int numberPounds) {
		for(int i = 0; i < numberPounds; i++) {
			Boolean itsOk = machineInstance.insertCoins(model, Coins.POUND);
			assertTrue(itsOk);
		}
		for(int i = 0; i < numberFifties; i++) {
			Boolean itsOk = machineInstance.insertCoins(model, Coins.FIFTY);
			assertTrue(itsOk);
		}
		for(int i = 0; i < numberTwenties; i++) {
			Boolean itsOk = machineInstance.insertCoins(model, Coins.TWENTY);
			assertTrue(itsOk);
		}
		for(int i = 0; i < numberTens; i++) {
			Boolean itsOk = machineInstance.insertCoins(model, Coins.TEN);
			assertTrue(itsOk);
		}
		return machineInstance.getBalance(model);
	}
	
	private void checkForCoin(Coins coin) {
		pennies.checkForCoin(coin);
	}
	
	private void resetMoneyCounter() {
		pennies.reset();
	}
	
}
