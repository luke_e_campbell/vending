package com.lukecampbell.vending.test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.lukecampbell.vending.test.controller.ExamplesControllerTest;
import com.lukecampbell.vending.test.machine.VendingMachineServiceTest;
import com.lukecampbell.vending.test.model.VendingModelTest;
import com.lukecampbell.vending.test.support.CurrencyTest;


@RunWith(Suite.class)
@Suite.SuiteClasses({
	VendingMachineServiceTest.class,
	ExamplesControllerTest.class,
	VendingModelTest.class,
	CurrencyTest.class
  
})

public class FeatureTestSuite {
  // the class remains empty,
  // used only as a holder for the above annotations
}