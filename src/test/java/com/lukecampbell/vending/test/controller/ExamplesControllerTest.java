package com.lukecampbell.vending.test.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.lukecampbell.vending.application.controller.ExamplesController;

@RunWith(SpringRunner.class)
@WebMvcTest(ExamplesController.class)
public class ExamplesControllerTest {

	
	@Autowired
    private MockMvc mvc;
	
	 @Test
	 public void testingGetResultsIn302Status() throws Exception 
	 {
		 	// run out of time, but I'd plan to check the content of this controller to check that the content
		    // contains the right data.
	        this.mvc.perform(get("/exampleOne").accept(MediaType.TEXT_HTML))
	        .andExpect(status().is(200));
	 }
}
