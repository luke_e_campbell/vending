<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>

	<h2> Report </h2>
	<ul>
		<li>Test: ${testName }</li>
		<li>Purchased Once: ${purchased }</li>
		<li>Purchased Twice: ${purchasedTwice }</li>
	</ul>
	
<%@ include file="common/footer.jspf"%>