package com.lukecampbell.vending.application.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.lukecampbell.vending.application.bo.CashContainer;
import com.lukecampbell.vending.model.VendingModel;
import com.lukecampbell.vending.product.Product;
import com.lukecampbell.vending.service.iface.IVendingMachineService;
import com.lukecampbell.vending.support.Constants;
import com.lukecampbell.vending.support.DemoConstants;
import com.lukecampbell.vending.support.PennyCounter;
import com.lukecampbell.vending.support.Currency.Coins;

@Controller
@EnableAutoConfiguration
public class ExamplesController {
private final static Logger logger = Logger.getLogger(VendingController.class);
    
    private IVendingMachineService service;
    
    private final String product1text = DemoConstants.product1text;
	private final int product1Cost = DemoConstants.product1Cost;
	private final String product2text = DemoConstants.product2text;
	private final int product2Cost = DemoConstants.product2Cost;
	private final String product3text = DemoConstants.product3text;
	private final int product3Cost = DemoConstants.product3Cost;
   
    public ExamplesController(IVendingMachineService service) {
        this.service = service;
    } 
    
    public void setUp(VendingModel model) {
    	Product product1 = new Product(product1text, product1Cost);
    	Product product2 = new Product(product2text, product2Cost);
    	Product product3 = new Product(product3text, product3Cost);
		List<Product> list = new ArrayList<Product>();
		list.add(product1);
		list.add(product2);
		list.add(product3);
		service.addProducts(model, list);
		
		
    }
    
    @RequestMapping("/exampleOne")
    public String firstExample(ModelMap modelMap, HttpSession httpSession) {
        VendingModel model = (VendingModel) httpSession.getAttribute(Constants.MODEL);
        checkModel(model);
        setUp(model);
        service.setOn(model);
		int numberPennies = 0;
		int numberTens = 1;
		int numberTwenties = 0;
		int numberFifties = 1;
		int numberPounds = 0;
		
		int balance = makeBalance(model, numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		
		int expectedBalance = Coins.TEN.value()+Coins.FIFTY.value();
		modelMap.addAttribute("testName", "Example One");
		modelMap.addAttribute("balance", expectedBalance);
		modelMap.addAttribute("expectedBalance", expectedBalance);
        return "exampleOne";
    }
    
    @RequestMapping("/exampleTwo")
    public String secondExample(ModelMap modelMap, HttpSession httpSession) {
        VendingModel model = (VendingModel) httpSession.getAttribute(Constants.MODEL);
        PennyCounter pennies = new PennyCounter();
        checkModel(model);
        setUp(model);
        service.setOn(model);
		int numberPennies = 0;
		int numberTens = 2;
		int numberTwenties = 0;
		int numberFifties = 1;
		int numberPounds = 0;
		int balance = makeBalance(model, numberPennies, numberTens, numberTwenties, numberFifties, numberPounds);
		Boolean purchased = service.purchaseProduct(model, product1text);
		int expectedBalance = balance - product1Cost;
		Optional<List<Coins>> coinsRefundedContainer = service.refundCoins(model);
		List<Coins> coinsRefunded = coinsRefundedContainer.get();
		//resetMoneyCounter(model);
		for(Coins coin: coinsRefunded) {
			checkForCoin(coin);
		}
		CashContainer cash = retrieveResultsFromMoneyCounter(pennies);
		// shouldn't be able to purchase twice
		Boolean purchasedTwice = service.purchaseProduct(model, product1text);
		modelMap.addAttribute("cash", cash);
		modelMap.addAttribute("purchasedOnce", purchased);
		modelMap.addAttribute("purchasedTwice", purchasedTwice);
		modelMap.addAttribute("testName", "Example Two");
		modelMap.addAttribute("balance", expectedBalance);
		
		modelMap.addAttribute("expectedBalance", expectedBalance);
        return "exampleTwo";
    }
    
    private void checkModel(VendingModel model) {
    	if(null == model) {
    		throw new IllegalArgumentException("Session has expired");
    	}
    }
    
    @ExceptionHandler(IllegalArgumentException.class)
    public ModelAndView illegalArgumentExceptionHandler(Exception exception, HttpServletRequest request) 
    {
        ModelAndView mav = new ModelAndView();
        ModelMap model = mav.getModelMap();
        
        mav.setViewName("welcome");
        return mav;
 
    }
    
    private CashContainer retrieveResultsFromMoneyCounter(PennyCounter pennies) {
    	boolean changeContainsTen = pennies.isContainsTen();
    	boolean changeContainsFifty = pennies.isContainsFifty();
    	boolean changeContainsTwenty = pennies.isContainsTwenty();
    	boolean changeContainsPound = pennies.isContainsPound();
    	boolean changeContainsPenny = pennies.isContainsPenny();
    	int countPenny = pennies.getCountPenny();
    	int countTen = pennies.getCountTen();
    	int countTwenty = pennies.getCountTwenty();
    	int countFifty = pennies.getCountFifty();
    	int countPound = pennies.getCountPound();
    	
    	CashContainer cash = new CashContainer(countPenny, countTen, countTwenty, countFifty, countPound, changeContainsPenny, changeContainsTen, changeContainsTwenty, changeContainsFifty, changeContainsPound);
    	return cash;
    }
	
	private int makeBalance(VendingModel model, int numberPennies, int numberTens, int numberTwenties, int numberFifties,
			int numberPounds) {
		for(int i = 0; i < numberPounds; i++) {
			Boolean itsOk = service.insertCoins(model, Coins.POUND);
		}
		for(int i = 0; i < numberFifties; i++) {
			Boolean itsOk = service.insertCoins(model, Coins.FIFTY);
		}
		for(int i = 0; i < numberTwenties; i++) {
			Boolean itsOk = service.insertCoins(model, Coins.TWENTY);
		}
		for(int i = 0; i < numberTens; i++) {
			Boolean itsOk = service.insertCoins(model, Coins.TEN);
		}
		return service.getBalance(model);
	}
	
	private PennyCounter checkForCoin(Coins coin) {
		PennyCounter pennies = new PennyCounter();
		pennies.checkForCoin(coin);
		return pennies;
	}
	
	private PennyCounter resetMoneyCounter(PennyCounter pennies) {
		pennies.reset();
		return pennies;
	}
    
}
