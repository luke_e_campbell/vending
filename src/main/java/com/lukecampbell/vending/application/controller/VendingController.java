package com.lukecampbell.vending.application.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.TypeMismatchException;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.lukecampbell.vending.model.VendingModel;
import com.lukecampbell.vending.service.iface.IVendingMachineService;
import com.lukecampbell.vending.support.Constants;


@Controller
@EnableAutoConfiguration
public class VendingController
{
    private final static Logger logger = Logger.getLogger(VendingController.class);
    
    private IVendingMachineService service;
   
    public VendingController(IVendingMachineService service) {
        this.service = service;
    } 
    
    @RequestMapping("/")
    public String showPavings(ModelMap model, HttpSession httpSession) {
        VendingModel vendingModel = new VendingModel();
        httpSession.setAttribute(Constants.MODEL, vendingModel);
        return "welcome";
    }
}
