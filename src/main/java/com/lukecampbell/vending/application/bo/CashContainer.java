package com.lukecampbell.vending.application.bo;

public class CashContainer {
	boolean changeContainsTen = false;
	boolean changeContainsFifty = false;
	boolean changeContainsTwenty = false;
	boolean changeContainsPound = false;
	boolean changeContainsPenny = false;
	private int countPound;
	private int countFifty;
	private int countTwenty;
	private int countTen;
	private int countPenny;
	
	public CashContainer(int countPenny2, int countTen2, int countTwenty2, int countFifty2,
			int countPound2, boolean changeContainsPenny2, boolean changeContainsTen2, boolean changeContainsTwenty2,
			boolean changeContainsFifty2, boolean changeContainsPound2) {
		countPenny = countPenny2;
		countTen = countTen2;
		countTwenty = countTwenty2;
		countFifty = countFifty2;
		countPound = countPound2;
		changeContainsPenny = changeContainsPenny2;
		changeContainsTen = changeContainsTen2;
		changeContainsTwenty = changeContainsTwenty2;
		changeContainsFifty = changeContainsFifty2;
		changeContainsPound = changeContainsPound2;	
	}
	
	public boolean isChangeContainsTen() {
		return changeContainsTen;
	}
	public void setChangeContainsTen(boolean changeContainsTen) {
		this.changeContainsTen = changeContainsTen;
	}
	public boolean isChangeContainsFifty() {
		return changeContainsFifty;
	}
	public void setChangeContainsFifty(boolean changeContainsFifty) {
		this.changeContainsFifty = changeContainsFifty;
	}
	public boolean isChangeContainsTwenty() {
		return changeContainsTwenty;
	}
	public void setChangeContainsTwenty(boolean changeContainsTwenty) {
		this.changeContainsTwenty = changeContainsTwenty;
	}
	public boolean isChangeContainsPound() {
		return changeContainsPound;
	}
	public void setChangeContainsPound(boolean changeContainsPound) {
		this.changeContainsPound = changeContainsPound;
	}
	public boolean isChangeContainsPenny() {
		return changeContainsPenny;
	}
	public void setChangeContainsPenny(boolean changeContainsPenny) {
		this.changeContainsPenny = changeContainsPenny;
	}
	public int getCountPound() {
		return countPound;
	}
	public void setCountPound(int countPound) {
		this.countPound = countPound;
	}
	public int getCountFifty() {
		return countFifty;
	}
	public void setCountFifty(int countFifty) {
		this.countFifty = countFifty;
	}
	public int getCountTwenty() {
		return countTwenty;
	}
	public void setCountTwenty(int countTwenty) {
		this.countTwenty = countTwenty;
	}
	public int getCountTen() {
		return countTen;
	}
	public void setCountTen(int countTen) {
		this.countTen = countTen;
	}
	public int getCountPenny() {
		return countPenny;
	}
	public void setCountPenny(int countPenny) {
		this.countPenny = countPenny;
	}
	
	
}
