package com.lukecampbell.vending.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.lukecampbell.vending.product.Product;
import com.lukecampbell.vending.support.Currency;
import com.lukecampbell.vending.support.Currency.Coins;



public class VendingModel {
	private Boolean onState = false;
	private List<Coins> listCoins;
	private List<Product> listProducts;
	private HashMap<String, Product> productMap;
	private int currentBalance;
	
	
	private void init() {
		listCoins = new ArrayList<Coins>();
		listProducts = new ArrayList<Product>();
		productMap = new HashMap<String, Product>();
		currentBalance = 0;
		setOff();
	}
	
	public VendingModel() {
		super();
		init();
	}
	
	public VendingModel(Product product1, Product product2, Product product3) {
		super();
		init();
		this.addProduct(product1);
		this.addProduct(product2);
		this.addProduct(product3);
		
	}
	
	public VendingModel(ArrayList<Product> products) {
		for(Product p: products) {
			this.addProduct(p);
		}
	}
	
	public void addProduct(Product p) {
		if(null == p) {
			return;
		}
		if(productMap.containsKey(p.getProductName())) {
			return;
		}
		listProducts.add(p);
		productMap.put(p.getProductName(), p);
	}

	public List<Product> getProducts() {
		if(onState) {
			return listProducts;
		}
		else {
			return new ArrayList<Product>();
		}
	}

	public boolean isOn() {
		return onState;
	}
	
	public void setOn() {
		onState = true;
	}
	
	public void setOff() {
		onState = false;
	}

	public Boolean insertCoins(Coins coin) {
		if(onState == false) {
			return onState;
		}
		if(Currency.isValid(coin)) {
			addBalance(coin);
			return true;
		}
		else {
			return false;
		}
	}

	public void addBalance(Coins coin) {
		
		listCoins.add(coin);
		currentBalance += coin.value();
		
	}

	public int getBalance() {
		return currentBalance;
	}

	public int refundBalance() {
		return currentBalance;
		
	}

	public Optional<List<Coins>> refundCoins() {
		if(isOn()) {
		  List<Coins> returnList = Currency.getBalance(currentBalance);
		  return Optional.of(returnList);
		}
		else {
			return Optional.empty();
		}
	}

	public Boolean purchaseProduct(String productText) {
		if(onState == false) {
			return onState;
		}
		Product purchased = productMap.get(productText);
		if(null == purchased) {
			return false;
		}
		
		int productCost = purchased.getCost();
		if(currentBalance < productCost) {
			return false;
		}
		currentBalance = currentBalance - productCost;
		return true;
	}
}
