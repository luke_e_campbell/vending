package com.lukecampbell.vending.support;

import com.lukecampbell.vending.support.Currency.Coins;

public class PennyCounter {
	private boolean containsTen = false;
	private boolean containsFifty = false;
	private boolean containsTwenty = false;
	private boolean containsPound = false;
	private boolean containsPenny = false;
	private int countPound;
	private int countFifty;
	private int countTwenty;
	private int countTen;
	private int countPenny;
	
	public void PennyCounter() {
		reset();
	}
	
	public void reset() {
		containsTen = false;
		containsFifty = false;
		containsTwenty = false;
		containsPound = false;
		containsPenny = false;
		countPound = 0;
		countFifty = 0;
		countTwenty = 0;
		countTen = 0;
		countPenny = 0;
	}
	
	public void checkForCoin(Coins coin) {
		
		if(coin.value() == Coins.POUND.value()) {
			containsPound = true;
			countPound++;
		}
		
		if(coin.value() == Coins.FIFTY.value()) {
			containsFifty = true;
			countFifty++;
		}
		
		if(coin.value() == Coins.TWENTY.value()) {
			containsTwenty = true;
			countTwenty++;
		}
		
		if(coin.value() == Coins.TEN.value()) {
			containsTen = true;
			countTen++;
		}
		
		if(coin.value() == Coins.PENNY.value()) {
			containsPenny = true;
			countPenny++;
		}
		
	}
	
	public int makeBalance(int numberPennies, int numberTens, int numberTwenties, int numberFifties,
			int numberPounds) {
		int balance = Coins.PENNY.value()*numberPennies;
		balance += Coins.TEN.value()*numberTens;
		balance += Coins.TWENTY.value()*numberTwenties;
		balance += Coins.FIFTY.value()*numberFifties;
		balance += Coins.POUND.value()*numberPounds;
		return balance;
	}
	
	public boolean isContainsTen() {
		return containsTen;
	}

	public void setContainsTen(boolean containsTen) {
		this.containsTen = containsTen;
	}

	public boolean isContainsFifty() {
		return containsFifty;
	}

	public void setContainsFifty(boolean containsFifty) {
		this.containsFifty = containsFifty;
	}

	public boolean isContainsTwenty() {
		return containsTwenty;
	}

	public void setContainsTwenty(boolean containsTwenty) {
		this.containsTwenty = containsTwenty;
	}

	public boolean isContainsPound() {
		return containsPound;
	}

	public void setContainsPound(boolean containsPound) {
		this.containsPound = containsPound;
	}

	public boolean isContainsPenny() {
		return containsPenny;
	}

	public void setContainsPenny(boolean containsPenny) {
		this.containsPenny = containsPenny;
	}

	public int getCountPound() {
		return countPound;
	}

	public void setCountPound(int countPound) {
		this.countPound = countPound;
	}

	public int getCountFifty() {
		return countFifty;
	}

	public void setCountFifty(int countFifty) {
		this.countFifty = countFifty;
	}

	public int getCountTwenty() {
		return countTwenty;
	}

	public void setCountTwenty(int countTwenty) {
		this.countTwenty = countTwenty;
	}

	public int getCountTen() {
		return countTen;
	}

	public void setCountTen(int countTen) {
		this.countTen = countTen;
	}

	public int getCountPenny() {
		return countPenny;
	}

	public void setCountPenny(int countPenny) {
		this.countPenny = countPenny;
	}
	
	
}
