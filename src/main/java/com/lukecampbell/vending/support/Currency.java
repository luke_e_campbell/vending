package com.lukecampbell.vending.support;

import java.util.ArrayList;

import com.lukecampbell.vending.support.Currency.Coins;

public class Currency {
	public enum Coins { 
	  PENNY(1), TEN(10), TWENTY(20), FIFTY(50), POUND(100); 
	  private int value; 
	  private Coins(int value) { 
		  this.value = value; 
	  }
	  
	  @Override 
	  public String toString() {  
		  String returnValue = "";
		  switch (this) { 
		    case PENNY: returnValue = "One Pence Piece: " + value; break; 
		  	case TEN: returnValue = "Ten Pence Piece: " + value; break; 
		  	case TWENTY: returnValue = "Twenty Pence Piece: " + value; break; 
		  	case FIFTY: returnValue = "Fifty Pence Piece: " + value; break; 
		  	case POUND: returnValue = "Pound Coin: " + value; 
		  } 
		  return returnValue; 
	  }
	  
	  public int value() {
		  return this.value;
	  }

	}; 
	
	public static ArrayList<Coins> getBalance(int balanceRequested) {
		int remainder = balanceRequested;
		ArrayList<Coins> coins = new ArrayList<Coins>();
		int numberHundred = balanceRequested / Coins.POUND.value();
		coins = Currency.addNumberCoins(coins, numberHundred, Coins.POUND);	
		remainder = balanceRequested - (numberHundred * Coins.POUND.value());
		
		int numberFifty = remainder / Coins.FIFTY.value();
		coins = Currency.addNumberCoins(coins, numberFifty, Coins.FIFTY);
		remainder = remainder - (numberFifty * Coins.FIFTY.value());
		int numberTwenty = remainder / Coins.TWENTY.value();
		coins = Currency.addNumberCoins(coins, numberTwenty, Coins.TWENTY);	
		remainder = remainder - (numberTwenty * Coins.TWENTY.value());
		int numberTen = remainder / Coins.TEN.value();
		coins = Currency.addNumberCoins(coins, numberTen, Coins.TEN);
		remainder = remainder - (numberTen * Coins.TEN.value());
		int remainingPennies = remainder / Coins.PENNY.value();
		coins = Currency.addNumberCoins(coins, remainingPennies, Coins.PENNY);
		return coins;
	}

	private static ArrayList<Coins> addNumberCoins(ArrayList<Coins> coins, int numberHundred, Coins pound) {
		int numberCoins = (int) numberHundred;
		for(int i = 0; i < numberCoins; i++) {
			coins.add(pound);
		}
		return coins;
	}

	public static boolean isValid(Coins coin) {
		Boolean valid = false;
		switch (coin) { 
		    case PENNY: valid = false; break; 
		  	case TEN: 
		  	case TWENTY: 
		  	case FIFTY: 
		  	case POUND: valid=true; break; 
	    }
		return valid;
	}

	
}
