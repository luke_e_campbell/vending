package com.lukecampbell.vending.support;

public class DemoConstants {
	public static final String product1text="Product A";
	public static final int product1Cost = 60;
	public static final String product2text="Product B";
	public static final int product2Cost = 100;
	public static final String product3text="Product C";
	public static final int product3Cost = 170;
}
