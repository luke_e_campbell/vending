package com.lukecampbell.vending.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.lukecampbell.vending.model.VendingModel;
import com.lukecampbell.vending.product.Product;
import com.lukecampbell.vending.service.iface.IVendingMachineService;
import com.lukecampbell.vending.support.Currency;
import com.lukecampbell.vending.support.Currency.Coins;
import org.springframework.stereotype.Service;
/**
 * Encapsulates the state of a vending machine and the operations that can be performed on it
 */


@Service
public class VendingMachineService implements IVendingMachineService {
	
	// this isn't thread safe -- it's just for the demo.
	// Since services in Spring run as singletons.
	
	
	public VendingMachineService() {
		super();
	}
	
	public void addProducts(VendingModel model, List<Product> products) {
		for(Product p: products) {
			model.addProduct(p);
		}
	}
	
	private void addProduct(VendingModel model, Product p) {
		model.addProduct(p);
	}

	public List<Product> getProducts(VendingModel model) {
		return model.getProducts();
	}

	public boolean isOn(VendingModel model) {
		return model.isOn();
	}
	
	public void setOn(VendingModel model) {
		model.setOn();
	}
	
	public void setOff(VendingModel model) {
		model.setOff();
	}

	public Boolean insertCoins(VendingModel model, Coins coin) {
		return model.insertCoins(coin);
	}

	private void addBalance(VendingModel model, Coins coin) {
		
		model.addBalance(coin);
		
	}

	public int getBalance(VendingModel model) {
		return model.getBalance();
	}

	public int refundBalance(VendingModel model) {
		return model.refundBalance();
		
	}

	public Optional<List<Coins>> refundCoins(VendingModel model) {
		return model.refundCoins();
	}

	public Boolean purchaseProduct(VendingModel model, String productText) {
		return model.purchaseProduct(productText);
	}

	
	
	
}
