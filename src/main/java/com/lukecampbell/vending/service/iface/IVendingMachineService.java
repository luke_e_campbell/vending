package com.lukecampbell.vending.service.iface;

import java.util.List;
import java.util.Optional;

import com.lukecampbell.vending.model.VendingModel;
import com.lukecampbell.vending.product.Product;
import com.lukecampbell.vending.support.Currency;
import com.lukecampbell.vending.support.Currency.Coins;

public interface IVendingMachineService {
	
	public abstract void addProducts(VendingModel model, List<Product> products);
	
	public abstract boolean isOn(VendingModel model);
	
	public abstract void setOn(VendingModel model);
	
	public abstract void setOff(VendingModel model);

	public abstract Boolean insertCoins(VendingModel model, Coins coin);
	
	public abstract int getBalance(VendingModel model);

	public abstract int refundBalance(VendingModel model);

	public abstract Optional<List<Coins>> refundCoins(VendingModel model);

	public abstract List<Product> getProducts(VendingModel model);

	public abstract Boolean purchaseProduct(VendingModel model, String productText);
}
