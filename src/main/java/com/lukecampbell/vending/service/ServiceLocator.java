package com.lukecampbell.vending.service;

import java.util.ArrayList;

import com.lukecampbell.vending.product.Product;
import com.lukecampbell.vending.service.iface.IVendingMachineService;
import com.lukecampbell.vending.service.impl.VendingMachineService;

public class ServiceLocator {
	public static IVendingMachineService getVendingMachine() {
		return new VendingMachineService();
	}

	public static IVendingMachineService getVendingMachine(Product product1, Product product2, Product product3) {
		return new VendingMachineService();
	}
	
	public static IVendingMachineService getVendingMachine(ArrayList<Product> products) {
		return new VendingMachineService();
	}
	
}
