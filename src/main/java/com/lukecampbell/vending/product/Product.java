package com.lukecampbell.vending.product;

public class Product {
	private String productName;
	private int cost;
	
	public Product(String n, int c) {
		super();
		this.setCost(c);
		this.setProductName(n);
	}
	
	public void setCost(int c) {
		this.cost = c;
	}
	
	public void setProductName(String n) {
		this.productName = n;
	}
	
	public int getCost() {
		return(this.cost);
	}
	
	public String getProductName() {
		return(this.productName);
	}
	
	public String toString() {
		return "Product "+this.productName+" - cost "+cost;
	}
}
